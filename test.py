def fizzbuzz(Set, m):

    check = 0
    for i in range(1,m):
        if m % i == 0 and i in Set:
            print(Set[i],end="")
            check=1
    if check == 0:
        print(m)

def main():

  Sets = {}
  fname=input()
  with open(fname) as f:
      for l in f:
        if ":" in l:
            (key, val) = l.split(":")
            Sets[int(key)] = val
        else:
            m=int(l)
  f.close()
  fizzbuzz(Sets,m)
  
if __name__ == "__main__":
    main()